import {ActionTypes} from '../constants/ActionTypes'

export const setBoards=(boards)=>{
  return {
    type:ActionTypes.SET_BOARDS,
    payload:boards,
  }
}
export const setLoader=(isLoading,error)=>{
  return{
    type:ActionTypes.SET_LOADING,
    payload:{isLoading,error},
  }
}


export const addNewBoard=(board)=>{
  return {
    type:ActionTypes.ADD_NEW_BOARD,
    payload:board,
  }
}


export const removeSelectedBoard=(id)=>{
  return {
    type:ActionTypes.REMOVE_SELECTED_BOARD,
    payload:id,
  }
}


export const setLists=(boardData)=>{
  return {
    type:ActionTypes.SET_LISTS,
    payload:boardData,
  }
}

export const getAllCards = (data,id)=>{
  return {
    type:ActionTypes.GET_ALL_CARDS,
    payload:[id,data]
  }
}

export const addNewList=(list)=>{
  console.log("new List");
  return {
    type:ActionTypes.ADD_NEW_LIST,
    payload:list,
  }
}


export const removeSelectedList=(id)=>{
  return {
    type:ActionTypes.REMOVE_SELECTED_LIST,
    payload:id,
  }
}


export const addCard=(card)=>{
  return {
    type:ActionTypes.ADD_CARD,
    payload:card,
  }
}


export const removeSelectedCard=(cardId,listId)=>{
  return {
    type:ActionTypes.REMOVE_SELECTED_CARD,
    payload:{cardId,listId},
  }
}



export const updateTitle = (id,newName)=>{
  return {
    type:ActionTypes.UPDATE_TITLE,
    payload:{id,newName},
  }
}

export const updateCardText = (response)=>{
  return {
    type:ActionTypes.UPDATE_CARD,
    payload:response,
  }
} 


