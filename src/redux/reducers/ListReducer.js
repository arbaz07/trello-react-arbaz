import { ActionTypes } from "../constants/ActionTypes";

export const listReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_LISTS:
      return {
        ...state,
        allList: payload,
      };



    case ActionTypes.ADD_NEW_LIST:
      console.log("adding list ", payload);
      return {
        ...state,
        allList: [...state.allList, payload],
      };



    case ActionTypes.REMOVE_SELECTED_LIST:
      return {
        ...state,
        allList: state.allList.filter((list) => list.id !== payload),
      };



    case ActionTypes.GET_ALL_CARDS:
      return {
        ...state,
        allList: state.allList.map((list) => {
          if (list.id === payload[0]) {
            return {
              ...list,
              allCards: payload[1],
            };
          } else {
            return list;
          }
        }),
      };



    case ActionTypes.ADD_CARD:
      return {
        ...state,
        allList: state.allList.map((item) => {
          if (item.id === payload.idList) {
            console.log("item.allCards :>> ", item.allCards);
            return {
              ...item,
              allCards: item.allCards.concat([payload]),
            };
          } else {
            return item;
          }
        }),
      };



    case ActionTypes.REMOVE_SELECTED_CARD:
      return {
        ...state,
        allList: state.allList.map((item) => {
          if (item.id === payload.listId) {
            return {
              ...item,
              allCards: item.allCards.filter(
                (card) => card.id !== payload.cardId
              ),
            };
          } else {
            return item;
          }
        }),
      };


    case ActionTypes.UPDATE_TITLE:
      return{
        ...state,
        allList:state.allList.map(list=>{
          if(list.id===payload.id){
            console.log(state);
            return {
              ...list,
              name:payload.newName,
            }
          }else{
            return list
          }
        })
      }


    case ActionTypes.UPDATE_CARD:
      return {
        ...state,
        allList:state.allList.map(list=>{
          if (list.id === payload.idList) {
            return {
              ...list,
              allCards: list.allCards.map(card=>{
                if(payload.id===card.id){
                  return {
                    ...card,
                    name:payload.name,
                  }
                }else{
                  return card;
                }  
              }),
            };
          } else {
            return list;
          }
        })
      }


    default:
      return state;
  }
};
