import { ActionTypes } from "../constants/ActionTypes";

let initialState = {
  boardsReducer:{
    isLoading:true,
    errorRaised:false,
  }
  
}

export const boardsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_BOARDS:
      return {
        allBoards: [...payload],
        isLoading:false,
        errorRaised:false,
      };
    case ActionTypes.SET_LOADING:
      return{
        isLoading:true,
        errorRaised:false,
      }


    case ActionTypes.REMOVE_SELECTED_BOARD:
      return {
        allBoards: state.allBoards.filter((board) => board.id !== payload),
      };



    case ActionTypes.ADD_NEW_BOARD:
      return {
        allBoards: [...state.allBoards, payload],
      };



    default:
      return state;
  }
};
