import { combineReducers } from "redux";
import {boardsReducer} from './BoardsReducers'
import {listReducer} from './ListReducer'

const reducers = combineReducers(
  {
    boardsReducer:boardsReducer,
    listReducer:listReducer,
  }
)
export default reducers;