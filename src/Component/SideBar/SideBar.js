// import React, { Component } from "react";
// import { Link } from "react-router-dom";
// import { fetchAllBards } from "../TrelloApi/TrelloApi";
// import store from "../../redux/Store";
// export default class SideBar extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       boards: [],
//       isLoading: true,
//       error: false,
//     };
//   }
//   async componentDidMount() {
//     try {
//       const data = await fetchAllBards();
//       console.log("data :>> ", data);
//       this.setState({
//         boards: data,
//         isLoading: false,
//         error: false,
//       });
//     } catch (err) {
//       this.setState({
//         error: true,
//         isLoading: false,
//       });
//     }
//   }
//   render() {
//     const boards = store.getState();
//     console.log('boards :>> ', boards);
//     return (
//       <div>
//         <nav
//           id="sidebarMenu"
//           className="collapse d-lg-block sidebar collapse bg-white"
//         >
//           <div className="position-sticky">
//             <div className="list-group list-group-flush mx-3 mt-4">
//               <Link
//                 to={"/"}
//                 className="list-group-item list-group-item-action py-2 ripple"
//               >
//                 Home
//               </Link>
//               {this.state.boards.map((board, index) => {
//                 let routId =
//                   "/" +
//                   board.id +
//                   "?backgroundImage=" +
//                   board.prefs.backgroundImage;
//                 return (
//                   <Link
//                     key={index}
//                     // to={routId}
//                     className="list-group-item list-group-item-action py-2 ripple"
//                   >
//                     <h5>{board.name}</h5>
//                   </Link>
//                 );
//               })}
//             </div>
//           </div>
//         </nav>
//       </div>
//     );
//   }
// }
