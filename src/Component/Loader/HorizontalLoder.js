import React from "react";
import "./LoaderStyle.css";

export default function HorizontalLoder() {
  return (
    <div className="horizontal-bar-wrap">
      <div className="bar1 bar"></div>
      <div className="bar4 bar"></div>
    </div>
  );
}
