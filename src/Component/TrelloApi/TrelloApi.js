import axios from "axios";

const baseURL = "https://api.trello.com/1"
const auth = "key=86ab6effb39a2bb58f2f68e8731f91b8&token=ATTAd475bc2beb291faae916b0f38c331e8510d52c27b6c56eecdb279aafecc93ae5B3628AB3"
// export let allboards;

export async function fetchAllBards(){
  const url=`${baseURL}/members/me/boards?${auth}`;
  const response = await axios.get(url)
  return response.data;
}

export async function deleteBoard(id){
  const url = `${baseURL}/boards/${id}?${auth}`
  const response = await axios.delete(url)
  return response.data;
}

export async function createNewBoard(boardName){
  const url = `${baseURL}/boards/?name=${boardName}&${auth}`
  const response = await axios.post(url);
  return response.data;
}



export async function getListForBoard(boardId){
  const url = `${baseURL}/boards/${boardId}/lists?${auth}`
  const response = await axios.get(url);
  return response.data;

}

export async function changeTitle(id,newName){
  const url = `${baseURL}/lists/${id}?${auth}`
  const response = await axios.put(url, { name: newName })
  return response.data;
}

export async  function getSingleList(id){
  const url = `${baseURL}/lists/${id}/cards?${auth}`
  const response = await axios.get(url);
  return response.data;

}

export async function removeListItem(id){
  const url = `${baseURL}/cards/${id}?${auth}`
  const response = await axios.delete(url)
  return response.data;
} 

export async function addItemInList(id,title){
  const url = `${baseURL}/cards?idList=${id}&${auth}` 
  const response = await  axios.post(url, {'name':title})
  return response.data;
}

export async function createNewList(title,boardId){
  const url = `${baseURL}/lists?name=${title}&idBoard=${boardId}&${auth}`
  const response = await axios.post(url)
  return response.data;
}

export async function archiveList(id){
  const url=`${baseURL}/lists/${id}/closed?${auth}`
  const response = await axios.put(url, { value: true })
  return response.data;
}


export async function updateCard(cardId, cardData){
  const url=`${baseURL}/cards/${cardId}?${auth}`
  const response = await axios.put(url,{ name: cardData })
  return response.data;
}
