import React, { Component } from "react";

export default class NewList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newListTitle: "",
    };
  }

  handleOnChange = (event) => {
    this.setState({
      newListTitle: event.target.value,
    });
  };

  handleSubmit=(event)=>{
    event.preventDefault()
    // console.log('event :>> ', event);
    this.props.newBoard(this.state.newListTitle)
    this.setState({
      newListTitle:''
    })
    document.getElementById('form-addItem').reset();
  }


  render() {
    return (
      <div
        className="m-3"
        style={{
          overflowX: "auto",
          backgroundColor: "#ebecf0",
          width: "300px",
        }}
      >
        <div className="list-group-item card">
          <form id='form-addItem' onSubmit={this.handleSubmit}>
            <input
              className="card-body mb-3  newList"
              style={{ width: "100%" }}
              placeholder={"Add Title!"}
              onChange={this.handleOnChange}
              required
            ></input>

            <button
            className="btn btn-primary"
            type="submit"
            value="Submit"  
            >
              Create New List!
            </button>
          </form>
        </div>
      </div>
    );
  }
}
