import React, { Component } from "react";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

export default class ListTitle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newTitle: "",
    };
  }

  handleChange = (event) => {
    this.setState({
      newTitle: event.target.value,
    });
  };

  handleSubmit=(e)=>{
    e.preventDefault()
    this.props.handleOnSubmit(this.props.data.id,this.state.newTitle);
    this.setState({
      newTitle:''
    })
  }
  render() {
    // console.log('this.props.data :>> ', this.props.data);
    return (
      <div
        className="list-group-item card"
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <form onSubmit={this.handleSubmit}>
          <input
            className="card-body"
            style={{
              width: "100%",
              fontSize: "18px",
              border: "none",
              color: "#7d7d7d",
              fontWeight: "bold",
            }}
            placeholder={this.props.data.name}
            onChange={this.handleChange}
          ></input>
        </form>
        <DeleteForeverIcon
          onClick={() => this.props.removeList(this.props.data.id)}
        />
      </div>
    );
  }
}
