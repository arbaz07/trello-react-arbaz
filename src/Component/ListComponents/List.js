import React, { Component } from "react";
import { LinearProgress } from "@mui/material";
import {connect} from 'react-redux'
import {
  getSingleList,
  removeListItem,
  addItemInList,
  updateCard,
} from "../TrelloApi/TrelloApi";
import Card from "./Card";
import ListTitle from "./ListTitle";
import AddCard from "./AddCard";
import { getAllCards ,addCard, removeSelectedCard,updateCardText} from "../../redux/action/DataAction";



class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      errorOccurred: false,
    };
  }

  componentDidMount = async () => {
    // console.log("component mount list");
    try {
      const data = await getSingleList(this.props.data.id);
      this.props.getAllCards(data,this.props.data.id)      
      
      this.setState({
        isLoaded: true,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        errorOccurred: true,
        isLoaded: true,
      });
    }
  };

  onRemoveCard = async (cardId) => {
    this.setState({
      isLoaded: false,
      errorOccurred: false,
    });
    try {
      await removeListItem(cardId);
      this.props.removeSelectedCard(cardId, this.props.data.id);
      this.setState({
        isLoaded: true,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        errorOccurred: true,
        isLoaded: true,
      });
    }
  };

  onInputBlur = (id, name) => {
    this.props.onTitleChange(id, name);
  };





  onAddCardToList = async (title) => {
    if (title === "") {
      return;
    }
    this.setState({
      isLoaded: false,
      errorOccurred: false,
    });
    try {
      const data = await addItemInList(this.props.data.id, title);
      this.props.addCard(data);
      this.setState({
        isLoaded: true,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        isLoaded: true,
        errorOccurred: true,
      });
    }
  };

  onArchive = async (id) => {
    this.setState({
      isLoaded: false,
    });
    try{
      await this.props.onArchiveList(id);
      this.setState({
        isLoaded: true,
      });
    }catch{
      this.setState({
        errorOccurred: true,
      });
    }
  };




  updateCard=async(cardId, text)=>{
    this.setState({
      isLoaded: false,
      errorOccurred: false,
    });
    try {
      const data = await updateCard(cardId, text);
      this.props.updateCard(data)
      this.setState({
        isLoaded: true,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        isLoaded: true,
        errorOccurred: true,
      });
    }
  }

  

  render() {
    const id = this.props.data.id

    const list = this.props.allList
    let cardsToDisplay;
    list.forEach(element => {
      if(element.id===id){
        cardsToDisplay=element;
      }
    });
    // console.log('cardsToDisplay :>> ', cardsToDisplay);

    return (
      <div className="list-group mt-5 g-3">
        {this.state.isLoaded ? (
          <div
            className="m-3"
            style={{
              overflowX: "auto",
              backgroundColor: "#ebecf0",
              width: "300px",
            }}
          >
            <div className="d-flex">
              <ListTitle
                data={this.props.data}
                handleOnSubmit={this.props.onTitleChange}
                removeList={this.onArchive}
              />
            </div>
            {cardsToDisplay.allCards.map((item) => {
              // console.log("item :>> ", item);
              return (
                <Card
                  key={item.id}
                  data={item}
                  removeListItem={this.onRemoveCard}
                  updateCard={this.updateCard}
                />
              );
            })}
            <AddCard
              addCard={this.onAddCardToList}
              listId={this.props.data.id}
            />
          </div>
        ) : (
          <LinearProgress style={{width:'320px'}} />
          
        )}
      </div>
    );
  }
}


function mapStateToProps(state){
  return {
    allList:state.listReducer.allList
  }
}

  
function mapMapDispatchToProps(dispatch){
  return{
    getAllCards:(data,id)=>{dispatch(getAllCards(data,id))},
    removeSelectedCard:(cardId,listId)=>{dispatch(removeSelectedCard(cardId,listId))},
    addCard:(data)=>{dispatch(addCard(data))},
    updateCard:(data)=>{dispatch(updateCardText(data))},
  }
}


export default connect(mapStateToProps,mapMapDispatchToProps)(List);