
import React, { Component } from 'react'


export default class Card extends Component {

  constructor(props){
    super(props)
    this.state={
      inputVisible:false,
      updatedText:'',
    }

  }


  newCardText=(text)=>{
    console.log('text :>> ', this.state.updatedText);
    this.setState({
      updatedText:text,
    })
  }

  toggle=()=>{
    console.log('object :>> inside toggle');
    this.setState({
      inputVisible:!this.state.inputVisible
    })
  }

  updateText= async(e)=>{
    e.preventDefault()
    const id = this.props.data.id
    const text = this.state.updatedText
    if(this.state.updatedText===''){
      return;
    }
    this.props.updateCard(id,text)

    
  }

  render() {
    // console.log('this.props.data.name :>> ', this.props.data.name);
    // console.log('this.state.u :>> ', this.state.u);
    return (
      <div
      className="list-group-item card m-3"
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        // backgroundColor:`linear-gradient(rgba(0,0,0,0.5), rgba(255,255,255,0))`,
      }}
    >
      {!this.state.inputVisible ?
      <p className="card-body p-0" style={{ marginBottom: 0 }} onClick={this.toggle}>
        {this.props.data.name}
      </p>:
      <form onSubmit={this.updateText}>
        <input className="card-body p-0" style={{ marginBottom: 0 }} onChange={(e)=>{this.newCardText(e.target.value)}} placeholder={this.props.data.name} onBlur={this.toggle}>
        </input>
      </form>}
      <button
        type="button"
        className="btn-close"
        aria-label="Close"
        onClick={() => {
          this.props.removeListItem(this.props.data.id);
        }}
      ></button>
    </div>
    )
  }
}
