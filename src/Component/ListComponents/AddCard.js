import React, { Component } from "react";

export default class AddCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardText: "",
    };
    // console.log("addlistItem");
  }

  handleChange = (event) => {
    this.setState({
      cardText: event.target.value,
    });
  };
  handleSubmit=(e)=>{
    e.preventDefault()
    this.props.addCard(this.state.cardText)
    const form = document.getElementById('form-addItem');
    // console.log('form :>> ', form);
  }
  render() {
    return (
      <div
        className="list-group-item card"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <form onSubmit={this.handleSubmit}>
          <input
            className="card-body"
            style={{ width: "100%" }}
            placeholder={"Add new Card"}
            onChange={this.handleChange}
            required
          ></input>
          <button
            className="btn btn-primary mt-3"
            type="submit"
            value="Submit"
          >
            Add Card!
          </button>
        </form>
      </div>
    );
  }
}
