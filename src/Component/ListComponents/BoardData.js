import React, { Component } from "react";
import { LinearProgress } from "@mui/material";
import { connect } from "react-redux";
import List from "./List";
import NewList from "./NewList";
import {
  getListForBoard,
  createNewList,
  archiveList,
  changeTitle,
} from "../TrelloApi/TrelloApi";
import { setLists,addNewList ,removeSelectedList, updateTitle} from "../../redux/action/DataAction";


class BoardData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      errorOccurred: false,
      boardId: this.props.match.params.id,
    };
  }


  backgroundImageUrl = () => {
    return this.props.location.search.split("=")[1];
  };



  onTitleChange = async (id, newName) => {
    if (newName === "") {
      return;
    }
    try {
      const res=await changeTitle(id, newName);
      this.props.updateTitle(id,newName)


      this.setState({
        isLoading: false,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        errorOccurred: true,
      });
    }
  };



  componentDidMount = async () => {
    try {
      const data = await getListForBoard(this.state.boardId);
      this.props.setLists(data);
      // store.dispatch(setLists(data))
      this.setState({
        isLoading: false,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        isLoading: false,
        errorOccurred: true,
      });
    }
  };



  onCreateNewList = async (title) => {
    if (title === "") {
      return;
    }
    try {
      const data = await createNewList(title, this.state.boardId); 
      this.props.addNewList(data);
      // store.dispatch(addNewList(data))
      this.setState({
        isLoading: false,
        errorOccurred: false,
      });
    } catch (error) {
      this.setState({
        isLoading: false,
        errorOccurred: true,
      });
    }
  };

  onArchiveList = async (id) => {
   
    try {
      await archiveList(id);
      this.props.removeSelectedList(id)


      this.setState({
        isLoading: false,
      });
    } catch (error) {
      this.setState({
        isLoading: false,
        errorOccurred: true,
      });
    }
  };



  render() {
    const allList = this.props.allList;
    return (
      <div
        style={{
          height: "100vh",
          width:'100vw',
          backgroundImage: `linear-gradient(rgba(0,0,0,0.5), rgba(255,255,255,0)),url(${this.backgroundImageUrl()})`,
          backgroundSize: "cover",
          color: "white",
        }}
      >
        {!this.state.isLoading ? (
          <div className="d-flex" style={{ overflow: "auto" }}>

            {allList.length===0 && <h1>No List Found!!!</h1>}
            {allList.map((list ) => {
              // console.log('list :>> ', list);
              return (
                <List
                  key={list.id}
                  data={list}
                  onTitleChange={this.onTitleChange}
                  onArchiveList={this.onArchiveList}
                />
              );
            })}
            <div className="list-group mt-5 g-3">
              <NewList newBoard={this.onCreateNewList} />
            </div>
          </div>
        ) : (
          <div>
            <LinearProgress />
          </div>
        )}
      </div>
    );
  }
}


function mapStateToProps(state){
  return {
    allList:state.listReducer.allList,
    // isLoading:state.boardsReducer.isLoading,
    // errorRaised:state.boardsReducer.errorRaised,
  }
}

function mapMapDispatchToProps(dispatch){
  return{
    setLists:(data)=>{dispatch(setLists(data))},
    updateTitle:(id,newName)=>{dispatch(updateTitle(id,newName))},
    addNewList:(data)=>{dispatch(addNewList(data))},
    removeSelectedList:(id)=>{dispatch(removeSelectedList(id))},
  }
}
export default connect(mapStateToProps,mapMapDispatchToProps)(BoardData);

// store.dispatch(addNewList(data))
// store.dispatch(removeSelectedList(id))

