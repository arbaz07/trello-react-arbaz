import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { LinearProgress } from "@mui/material";

import {
  fetchAllBards,
  deleteBoard,
  createNewBoard,
} from "../TrelloApi/TrelloApi";
import Board from "./Board";
import store from "../../redux/Store";
import {
  setBoards,
  removeSelectedBoard,
  addNewBoard,
} from "../../redux/action/DataAction";
import { connect } from 'react-redux';


class Boards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newBoardName: "",
    };
  }

  componentDidMount = async () => {

    try {
      const data = await fetchAllBards();
      console.log("Data: ",data);
      //it will set loading to false data to data, error to false
      this.props.setBoards(data)
     
    } catch (error) {
      // this.setState({
      //   errorRaised: true,
      //   isLoading: false,
      // });
    }
  };

  handleChange = (event) => {
    this.setState({ newBoardName: event.target.value });
  };

  onHandleDelete = async (id) => {
   
    try {
      await deleteBoard(id);

      //redux
      this.props.removeSelectedBoard(id)
      // store.dispatch(removeSelectedBoard(id));
   

      this.setState({
        // isLoading: false,
        boards: this.state.boards.filter((board) => board.id !== id),
      });
    } catch (error) {
      this.setState({
        errorRaised: true,
        isLoading: false,
      });
    }
  };

  onCreateNewBoard = async (e) => {
    e.preventDefault();
    if (this.state.newBoardName === "") {
      return;
    }
   
    try {
      const data = await createNewBoard(this.state.newBoardName);
      this.props.addNewBoard(data);
      
      this.setState({
        newBoardName: "",
      });
    } catch (error) {
      this.setState({
        errorRaised: true,
        isLoading: false,
      });
    }
  };

  render() {
    // console.log('this.props :>> ', this.props);
    const storeData = this.props.data
    if(storeData===undefined){
      return<div className="d-flex flex-row justify-center" style={{width:'100vw',height:'100vh'}}><h1 style={{fontSize:'100px', color:'reg',margin:'auto'}}>Loading...</h1></div>;
    }
    // console.log('this.props.isLoading :>> ', this.props.isLoading);

    return (
      <div>
        {this.props.errorRaised && <div className="d-flex flex-row justify-center" style={{width:'100vw',height:'100vh'}}><h1 style={{fontSize:'100px', color:'reg',margin:'auto'}}>Something went Wrong!...</h1></div>}
        {!this.props.isLoading && (
          <div className="all-boards">
            {storeData.map((board) => {
              let routId =
                "/" +
                board.id +
                "?backgroundImage=" +
                board.prefs.backgroundImage;
              return (
                <div
                  className="board-container"
                  key={board.id}
                  style={{
                    width: "350px",
                    backgroundColor: "#ced6d4",
                    borderRadius: "1rem",
                    margin: "2em",
                  }}
                >
                  <Link
                    to={routId}
                    className="routerLink"
                    style={{ textDecoration: "none" }}
                  >
                    <Board data={board} />
                  </Link>
                  <button
                    className="btn btn-danger m-3 mt-0 "
                    onClick={() => {
                      this.onHandleDelete(board.id);
                    }}
                  >
                    Delete Board
                  </button>
                </div>
              );
            })}
            <div
              className="board-container"
              style={{
                width: "350px",
                backgroundColor: "#ced6d4",
                borderRadius: "1rem",
                margin: "2em",
              }}
            >
              <div className="card m-3">
                <div className="card-body">
                  <form onSubmit={this.onCreateNewBoard}>
                    <input
                      style={{ width: "100%" }}
                      placeholder="Enter Title"
                      onChange={this.handleChange}
                    ></input>

                    <p className="card-text">
                      Sed ut perspiciatis unde omnis iste natus error sit
                      voluptatem accusantium doloremque laudantium, totam rem
                      aperiam.
                    </p>

                    <button
                      className="btn btn-primary"
                      type="submit"
                      value="Submit"
                    >
                      Create New Board
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}


function mapStateToProps(state){
  return {
    data:state.boardsReducer.allBoards,
    isLoading:state.boardsReducer.isLoading,
    errorRaised:state.boardsReducer.errorRaised,
  }
}

function mapMapDispatchToProps(dispatch){
  return{
    setBoards:(data)=>{dispatch(setBoards(data))},
    removeSelectedBoard:(id)=>{dispatch(removeSelectedBoard(id))},
    addNewBoard:(data)=>{dispatch(addNewBoard(data))},
  }
}
export default connect(mapStateToProps,mapMapDispatchToProps)(Boards);


