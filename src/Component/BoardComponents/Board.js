import React from "react";

export default function Board(props) {
  return (
    <div >
      <div
        className="card m-3 "
        style={{
          backgroundImage: `linear-gradient(rgba(0,0,0,0.5), rgba(255,255,255,0)),url(${props.data.prefs.backgroundImage})`,
          backgroundSize: "cover",
          color: "white",
          
        }}
      >
        <div className="card-body" style={{height:'15rem'}}>
          <h5 className="card-title" style={{fontSize:'2rem'}}>{props.data.name}</h5>
          <p className="card-text">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
          </p>
        </div>
      </div>
    </div>
  );
}
