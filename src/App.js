import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Boards from "./Component/BoardComponents/Boards";
import BoardData from "./Component/ListComponents/BoardData";
import Navbar from "./Component/NavbarComponent/Navbar";
// import SideBar from "./Component/SideBar/SideBar";

export default class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />

          <Switch>
            <div className="container-body">
                <Route exact path="/" component={Boards} />
                <Route path="/:id" component={BoardData} />
            </div>
          </Switch>
        </div>
      </Router>
    );
  }
}

